import logging
import sys

from provout.transfer.standard_transfer_object import StandardTransferObject


def setup_logger(standard_level: int) -> logging.Logger:
    root = logging.getLogger(__name__)
    root.setLevel(STANDARD_LOG_LEVEL)

    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s: %(message)s')
    handler.setFormatter(formatter)

    root.addHandler(handler)
    return root


STANDARD_LOG_LEVEL = logging.DEBUG
LOGGER = setup_logger(STANDARD_LOG_LEVEL)


def output(data_container: StandardTransferObject, **meta) -> None:
    """Outputs the data to the standard out stream.

    Args:
        data_container: a data container to represent data from
        **meta: variadic keyword parameters
                "log_level: int" keyword specifies the log level for the output
    """
    log_level = meta.get("log_level", STANDARD_LOG_LEVEL)
    LOGGER.log(log_level, str(data_container.data_dict))

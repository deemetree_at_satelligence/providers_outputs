from provout.transfer.standard_transfer_object import StandardTransferObject


def provide(n: int) -> StandardTransferObject:
    """Returns the n'th fibonacci number.

    Args:
        n: an integer seed for fibonacci numbers

    Returns:
        StandardTransferObject: an output object
    """
    return StandardTransferObject(get_fibonacci_dict(n))


def get_fibonacci_dict(n: int) -> dict:
    return dict(fib=fib(n))


def fib(n, computed={0: 0, 1: 1}):
    if n not in computed:
        computed[n] = fib(n-1, computed) + fib(n-2, computed)
    return computed[n]

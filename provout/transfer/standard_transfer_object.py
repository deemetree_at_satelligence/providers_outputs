class StandardTransferObject:
    def __init__(self, data):
        self.data = data

    @property
    def data_dict(self) -> dict:
        return dict(data=self.data)

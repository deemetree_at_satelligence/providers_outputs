from unittest import TestCase
from unittest.mock import MagicMock, patch

from provout.main import main


PATCH_FIBONACCI_PROVIDER = patch(
    'provout.main.fibonacci_provider', autospec=True)
PATCH_STDOUT_WRITER = patch(
    'provout.main.stdout_writer', autospec=True)
PATCH_TEXT_FILE_WRITER = patch(
    'provout.main.text_file_writer', autospec=True)


class MainTestCase(TestCase):
    """Test case for Main"""

    def setUp(self):
        self._mock_fibonacci_provider = PATCH_FIBONACCI_PROVIDER.start()
        self._mock_stdout_writer = PATCH_STDOUT_WRITER.start()
        self._mock_text_file_writer = PATCH_TEXT_FILE_WRITER.start()

        self._test_function = main

    def tearDown(self):
        PATCH_FIBONACCI_PROVIDER.stop()
        PATCH_STDOUT_WRITER.stop()
        PATCH_TEXT_FILE_WRITER.stop()

    def test_main_computes_25th_number_using_fibonacci_provider(self):
        expected_value = 25
        self._test_function()
        self._mock_fibonacci_provider.provide.assert_called_once_with(
            expected_value)

    def test_main_outputs_result_of_fibonacci_provider_to_each_writer(self):
        writers = [self._mock_stdout_writer, self._mock_text_file_writer]
        expected_data_container = "some_provider_result"

        self._mock_fibonacci_provider.provide.return_value = expected_data_container

        self._test_function()

        for writer in writers:
            writer.output.assert_called_once_with(expected_data_container)

    @patch('builtins.print')
    def test_main_prints_done(self, mock_print):
        self._test_function()
        mock_print.assert_called_once_with('done')
